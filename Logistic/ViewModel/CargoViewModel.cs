﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistic.ViewModel
{
    public class CargoViewModel
    {
        public ClientViewModel Client { get; set; }
        public ClientPayerVewModel ClientPayer { get; set; }
        public CargoOwnerViewModel CargoOwner { get; set; }
        
        public double CargoWeight { get; set; }
        public int CargoPlaces { get; set; }
        public string CargoRequestNumber { get; set; }

        public ClientViewModel[] Clients { get; set; }
        public ClientPayerVewModel[] ClientPayers { get; set; }
        public CargoOwnerViewModel[] CargoOwners { get; set; }
    }
}