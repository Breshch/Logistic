﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistic.ViewModel
{
    public class CargoOwnerViewModel
    {
        public int CargoOwnerId { get; set; }
        public string CargoOwnerName { get; set; }
        
    }
}
