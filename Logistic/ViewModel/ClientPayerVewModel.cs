﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistic.ViewModel
{
    public class ClientPayerVewModel
    {
        public int ClientPayerId { get; set; }
        public string ClientPayerName { get; set; }
    }
}
