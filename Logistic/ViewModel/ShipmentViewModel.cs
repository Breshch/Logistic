﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logistic.Model;

namespace Logistic.ViewModel
{
    public class ShipmentViewModel
    {
        public int Number { get; set; }
        public int ShipmentId { get; set; }
        public List<Cargo> Cargoes { get; set; }
        public string CargoRequestNumber { get; set; }
        public string CargoOwner { get; set; }
        public DateTime Date { get; set; }
        public string ForwarderName { get; set; }
        public string ForwarderCompanyName { get; set; }
        public double CarWeight { get; set; }
        public int CarPriceValue { get; set; }
        public string CarType { get; set; }
        public string ClientName { get; set; }
       
        public ShipmentViewModel()
        {
            // Cargo = new CargoViewModel();
        }

    }
}
