﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistic.ViewModel
{
    public class CarViewModel
    {
        public DateTime Date { get; set; }
        public string ForwarderName { get; set; }
        public string ForwarderCompanyName { get; set; }
        public string CarType { get; set; }
        public double CarWeight { get; set; }
        public int CarPriceValue { get; set; }
    }
}
