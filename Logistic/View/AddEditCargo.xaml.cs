﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Logistic.Model;
using Logistic.ViewModel;

namespace Logistic.View
{
    public partial class AddEditCargo : Window
    {
        public ObservableCollection<CargoViewModel> Cargoes;
        private bool _isCargoes = false;
        private int _shipmentId = 0;
        public AddEditCargo(int shipmentId)
        {
            InitializeComponent();

            LogisticContext lc = new LogisticContext();

            _shipmentId = shipmentId;
            var shipments = lc.Shipments;
            var shipment = shipments.First(x => x.Id == shipmentId);
            DatePickerDate.SelectedDate = shipment.Date.Date;
            DatePickerDate.DataContext = shipment;
            DatePickerDate.IsTodayHighlighted = true;

            var forwarders = lc.Forwarders.ToArray();
            var forwarder = forwarders.First(x => x.Id == shipment.ForwarderId);
            ComboboxForwarder.ItemsSource = forwarders;
            ComboboxForwarder.SelectedItem = forwarder;

            var forwarderCompanies = lc.ForwarderCompanies.ToArray();
            var forwarderCompany = forwarderCompanies.First(x => x.Id == forwarder.ForwarderCompanyId);
            ComboBoxForwarderCompany.ItemsSource = forwarderCompanies;
            ComboBoxForwarderCompany.SelectedItem = forwarderCompany;

            var carTypes = lc.CarTypes.ToArray();
            var carType = carTypes.First(x => x.Id == shipment.CarTypeId);
            ComboboxCar.ItemsSource = carTypes;
            ComboboxCar.SelectedItem = carType;

            var carWeights = lc.CarWeights.ToArray();
            var carWeight = carWeights.First(x => x.Id == shipment.CarWeight.Id);
            ComboboxCarWeight.ItemsSource = carWeights;
            ComboboxCarWeight.SelectedItem = carWeight;

            var carPrices = lc.CarPrices.ToArray();
            var carPrice = carPrices.First(x => x.Id == shipment.CarPriceId);
            ComboboxCarCost.ItemsSource = carPrices;
            ComboboxCarCost.SelectedItem = carPrice;


            ClientViewModel[] clients = lc.Clients.Select(x => new ClientViewModel
            {
                ClientId = x.Id,
                ClientName = x.ClientName
            }).ToArray();

            ClientPayerVewModel[] payers = lc.ClientPayers.Select(x => new ClientPayerVewModel
            {
                ClientPayerId = x.Id,
                ClientPayerName = x.ClientPayerName
            }).ToArray();

            CargoOwnerViewModel[] cargoOwners = lc.CargoOwners.Select(x => new CargoOwnerViewModel
            {
                CargoOwnerId = x.Id,
                CargoOwnerName = x.CargoOwnerName
            }).ToArray();

            var cargoes = lc.Cargoes.Include(x => x.CargoOwner).Where(x => x.ShipmentId == shipmentId).ToArray();

            List<CargoViewModel> cargoesViewModel = new List<CargoViewModel>();
            foreach (var cargo in cargoes)
            {
                var cargoViewModel = new CargoViewModel
                {
                    CargoRequestNumber = cargo.CargoRequestNumber,
                    CargoPlaces = cargo.CargoPlaces,
                    CargoWeight = cargo.CargoWeight,

                    CargoOwner = cargoOwners.First(x => x.CargoOwnerId == cargo.CargoOwnerId),
                    CargoOwners = cargoOwners,

                    Client = clients.First(x => x.ClientId == cargo.ClientId),
                    Clients = clients,

                    ClientPayer = payers.First(x => x.ClientPayerId == cargo.ClientPayerId),
                    ClientPayers = payers
                };
                cargoesViewModel.Add(cargoViewModel);
            }

            Cargoes = new ObservableCollection<CargoViewModel>(cargoesViewModel);
            _isCargoes = true;
            DataGridCargoes.ItemsSource = Cargoes;
        }

        public AddEditCargo()
        {
            InitializeComponent();

            LogisticContext lc = new LogisticContext();

            DatePickerDate.SelectedDate = DateTime.Now;
            DatePickerDate.IsTodayHighlighted = true;

            var forwarders = lc.Forwarders.ToArray();
            ComboboxForwarder.ItemsSource = forwarders;
            //ComboboxForwarder.SelectedItem = forwarders.First();

            var forwarderCompanies = lc.ForwarderCompanies.ToArray();
            ComboBoxForwarderCompany.ItemsSource = forwarderCompanies;
            //ComboBoxForwarderCompany.SelectedItem = forwarderCompanies.First();

            var carTypes = lc.CarTypes.ToArray();
            ComboboxCar.ItemsSource = carTypes;
            //ComboboxCar.SelectedItem = carTypes.First();

            var carWeights = lc.CarWeights.ToArray();
            ComboboxCarWeight.ItemsSource = carWeights;
            //ComboboxCarWeight.SelectedItem = carWeights.First();

            var carPrices = lc.CarPrices.ToArray();
            ComboboxCarCost.ItemsSource = carPrices;
            ComboboxCarCost.SelectedItem = carPrices.First();


            ClientViewModel[] clients = lc.Clients.Select(x => new ClientViewModel
            {
                ClientId = x.Id,
                ClientName = x.ClientName
            }).ToArray();

            ClientPayerVewModel[] payers = lc.ClientPayers.Select(x => new ClientPayerVewModel
            {
                ClientPayerId = x.Id,
                ClientPayerName = x.ClientPayerName
            }).ToArray();

            CargoOwnerViewModel[] cargoOwners = lc.CargoOwners.Select(x => new CargoOwnerViewModel
            {
                CargoOwnerId = x.Id,
                CargoOwnerName = x.CargoOwnerName
            }).ToArray();

            var cargoes = lc.Cargoes.Include(x => x.CargoOwner).ToArray();

            List<CargoViewModel> cargoesViewModel = new List<CargoViewModel>();
            foreach (var cargo in cargoes)
            {
                var cargoViewModel = new CargoViewModel
                {
                    CargoRequestNumber = cargo.CargoRequestNumber,
                    CargoPlaces = cargo.CargoPlaces,
                    CargoWeight = cargo.CargoWeight,

                    CargoOwner = cargoOwners.First(x => x.CargoOwnerId == cargo.CargoOwnerId),
                    CargoOwners = cargoOwners,

                    Client = clients.First(x => x.ClientId == cargo.ClientId),
                    Clients = clients,

                    ClientPayer = payers.First(x => x.ClientPayerId == cargo.ClientPayerId),
                    ClientPayers = payers
                };
                cargoesViewModel.Add(cargoViewModel);
            }

            Cargoes = new ObservableCollection<CargoViewModel>();
            DataGridCargoes.ItemsSource = Cargoes;
        }

        private void ButtonSaveAndExitForm_Click(object sender, RoutedEventArgs e)
        {
            Shipment shipment;
            LogisticContext lc = new LogisticContext();
            if (_shipmentId != 0)
            {

                shipment = lc.Shipments.First(x => x.Id == _shipmentId);
                lc.Shipments.Remove(shipment);
            }

            if (DatePickerDate.SelectedDate != null)
            {
                shipment = new Shipment
                {
                    Cargoes = Cargoes.Select(x => new Cargo
                    {
                        CargoOwnerId = x.CargoOwner.CargoOwnerId,
                        CargoPlaces = x.CargoPlaces,
                        CargoRequestNumber = x.CargoRequestNumber,
                        CargoWeight = x.CargoWeight,
                        ClientId = x.Client.ClientId,
                        ClientPayerId = x.ClientPayer.ClientPayerId
                    }).ToList(),
                    CarPriceId = ((CarPrice)ComboboxCarCost.SelectedItem).Id,
                    CarTypeId = ((CarType)ComboboxCar.SelectedItem).Id,
                    CarWeightId = ((CarWeight)ComboboxCarWeight.SelectedItem).Id,
                    Date = DatePickerDate.SelectedDate.Value,
                    ForwarderId = ((Forwarder)ComboboxForwarder.SelectedItem).Id,
                    ForwarderCompanyId = ((ForwarderCompany)ComboBoxForwarderCompany.SelectedItem).Id
                };
                lc.Shipments.Add(shipment);
            }
            lc.SaveChanges();
        }


        private void ButtonDeleteCargo_OnClick(object sender, RoutedEventArgs e)
        {
            if (DataGridCargoes.SelectedItems.Count == 0 || DataGridCargoes.SelectedItems.Count > 1)
            {
                return;
            }

            var selectedCargo = (DataGridCargoes.SelectedItem as CargoViewModel);
            Cargoes.Remove(selectedCargo);
        }

        private void ButtonAddCargo_OnClick(object sender, RoutedEventArgs e)
        {
            LogisticContext lc = new LogisticContext();

            ClientViewModel[] clients = lc.Clients.Select(x => new ClientViewModel
            {
                ClientId = x.Id,
                ClientName = x.ClientName
            }).ToArray();

            ClientPayerVewModel[] payers = lc.ClientPayers.Select(x => new ClientPayerVewModel
            {
                ClientPayerId = x.Id,
                ClientPayerName = x.ClientPayerName
            }).ToArray();

            CargoOwnerViewModel[] cargoOwners = lc.CargoOwners.Select(x => new CargoOwnerViewModel
            {
                CargoOwnerId = x.Id,
                CargoOwnerName = x.CargoOwnerName
            }).ToArray();

            Cargoes.Add(new CargoViewModel
            {
                Clients = clients,
                ClientPayers = payers,
                CargoOwners = cargoOwners
            });
        }

        private void ButtonClearForm_OnClick(object sender, RoutedEventArgs e)
        {

            LogisticContext lc = new LogisticContext();

            ComboboxForwarder.SelectedItem = null;
            ComboboxCarWeight.SelectedItem = null;
            ComboboxCarCost.SelectedItem = null;
            ComboboxCar.SelectedItem = null;
            DatePickerDate.SelectedDate = DateTime.Now;
            ComboBoxForwarderCompany.SelectedItem = null;

            ClientViewModel[] clients = lc.Clients.Select(x => new ClientViewModel
            {
                ClientId = x.Id,
                ClientName = x.ClientName
            }).ToArray();

            ClientPayerVewModel[] payers = lc.ClientPayers.Select(x => new ClientPayerVewModel
            {
                ClientPayerId = x.Id,
                ClientPayerName = x.ClientPayerName
            }).ToArray();

            CargoOwnerViewModel[] cargoOwners = lc.CargoOwners.Select(x => new CargoOwnerViewModel
            {
                CargoOwnerId = x.Id,
                CargoOwnerName = x.CargoOwnerName
            }).ToArray();

            Cargoes.Clear();

            Cargoes.Add(new CargoViewModel
            {
                Clients = clients,
                ClientPayers = payers,
                CargoOwners = cargoOwners
            });
            DataGridCargoes.ItemsSource = Cargoes;

        }

        private void ComboboxForwarder_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LogisticContext lc = new LogisticContext();
            var forwarder = ComboboxForwarder.SelectedItem as Forwarder;
            ComboboxForwarder.SelectedItem = forwarder.ForwarderName;

            var forwarderCompanies = lc.ForwarderCompanies.ToArray();
            ComboBoxForwarderCompany.SelectedItem = forwarderCompanies.First(x => x.Id == forwarder.ForwarderCompanyId).CompanyName;

            var carTypes = lc.CarTypes.ToArray();
            ComboboxCar.SelectedItem = carTypes.First( x => x.Id == forwarder.CarTypeId).Type;

            var carWeights = lc.CarWeights.ToArray();
            ComboboxCarWeight.SelectedItem = carWeights.First(x => x.Id == forwarder.CarWeightId).Weight;

            var carPrices = lc.CarPrices.ToArray();
            ComboboxCarCost.SelectedItem = carPrices.First(x => x.Id == forwarder.CarPriceId).Price;
        }
    }
}
