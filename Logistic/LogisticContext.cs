﻿using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Media.Animation;
using Logistic.Model;


namespace Logistic
{
    public class LogisticContext : DbContext
    {
        //ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;

        

        public LogisticContext() : base("LogisticContextExt")
        {
            Database.SetInitializer<LogisticContext>(null);
        }

        public DbSet<Manager> Managers { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<ClientPayer> ClientPayers { get; set; }
        public DbSet<CargoShipmentRout> CargoShipmentRouts { get; set; }
        public DbSet<CargoOwner> CargoOwners { get; set; }
        public DbSet<CarWeight> CarWeights { get; set; }
        public DbSet<Cargo> Cargoes { get; set; }
        public DbSet<ForwarderCompany> ForwarderCompanies { get; set; }
        public DbSet<Forwarder> Forwarders { get; set; }
        public DbSet<CarPrice> CarPrices { get; set; }
        public DbSet<CarType> CarTypes { get; set; }
        public DbSet<Shipment> Shipments { get; set; }
    }
}