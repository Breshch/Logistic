﻿using System.ComponentModel.DataAnnotations;

namespace Logistic.Model
{
    public class Manager
    {
        public int Id { get; set; }
        public string ManagerName { get; set; }
    }
}