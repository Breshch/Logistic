﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Logistic.Model
{
    public class CargoShipmentRout
    {
        public int Id { get; set; }
        public string ShipmentAddress { get; set; }
        public int ClientId { get; set; }

    }
}