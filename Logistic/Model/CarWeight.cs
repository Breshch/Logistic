﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistic.Model
{
    public class CarWeight
    {
        public int Id { get; set; }

        public double Weight { get; set; }
    }
}
