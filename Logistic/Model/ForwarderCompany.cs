﻿using System.ComponentModel.DataAnnotations;

namespace Logistic.Model
{
    public class ForwarderCompany
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
    }
}