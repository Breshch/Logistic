﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Windows.Documents;

namespace Logistic.Model
{
    public class Shipment
    {

        public int Id { get; set; }
        public DateTime Date { get; set; }
        public List<Cargo> Cargoes { get; set; }
        public int CarWeightId { get; set; }
        public CarWeight CarWeight { get; set; }
        public int ForwarderId { get; set; }
        public Forwarder Forwarder { get; set; }
        public int ForwarderCompanyId { get; set; }
        public ForwarderCompany ForwarderCompany { get; set; }
        public int CarPriceId { get; set; }
        public CarPrice CarPrice { get; set; }
        public int CarTypeId { get; set; }
        public CarType CarType { get; set; }
    }
}