﻿using System.ComponentModel.DataAnnotations;

namespace Logistic.Model
{
    public class CargoOwner
    {
        public int Id { get; set; }
        public string CargoOwnerName { get; set; }
    }
}