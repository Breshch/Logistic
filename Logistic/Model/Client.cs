﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Logistic.Model
{
    public class Client
    {
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string ClientPayerId { get; set; }
        public ClientPayer ClientPayer { get; set; }
        public int ManagerId { get; set; }
        public string ClientCredentional { get; set; }
    }
}
