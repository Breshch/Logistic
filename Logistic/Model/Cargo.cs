﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Migrations.Model;
using System.Windows.Documents;

namespace Logistic.Model
{
    public class Cargo
    {
        public int Id { get; set; }

        #region CargoInfo

        public int CargoOwnerId { get; set; }
        public CargoOwner CargoOwner { get; set; }
        public int ClientId { get; set; }
        public Client Client { get; set; }
        public int ClientPayerId { get; set; }
        public double CargoWeight { get; set; }
        public int CargoPlaces { get; set; }
        public string CargoRequestNumber { get; set; }
        public int ShipmentId { get; set; }
        public Shipment Shipment { get; set; }
       
        #endregion


    }
}