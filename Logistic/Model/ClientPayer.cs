﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistic.Model
{
    public class ClientPayer
    {
        public int Id { get; set; }
        public string ClientPayerName { get; set; }
        public int ClientPayerId { get; set; }

    }
}
