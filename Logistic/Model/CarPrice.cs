﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logistic.Model
{
    public class CarPrice
    {
        public int Id { get; set; }
        public int Price  { get; set; }

    }
}
