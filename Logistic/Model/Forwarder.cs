﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Logistic.Model
{
    public class Forwarder
    {
        public int Id { get; set; }
        public string ForwarderName { get; set; }
        public int ForwarderCompanyId { get; set; }
        public ForwarderCompany ForwarderCompany { get; set; }
        public int CarTypeId { get; set; }
        public int CarWeightId  { get; set; }
        public int CarPriceId { get; set; }
    }
}