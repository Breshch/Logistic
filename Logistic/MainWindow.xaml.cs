﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Logistic.Helpers;
using Logistic.Model;
using Logistic.ViewModel;
using System.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Input;
using Logistic.View;

namespace Logistic
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MakeDataGrid();
        }

        public void MakeDataGrid()
        {
            LogisticContext lc = new LogisticContext();

            var shipments = lc.Shipments.SelectMany(shipment => shipment.Cargoes.Select(cargo => new ShipmentViewModel
            {
                    ShipmentId = shipment.Id,
                    CarWeight = shipment.CarWeight.Weight,
                    Date = shipment.Date,
                    ForwarderName = shipment.Forwarder.ForwarderName,
                    ForwarderCompanyName = shipment.Forwarder.ForwarderCompany.CompanyName,
                    CarPriceValue = shipment.CarPrice.Price,
                    CarType = shipment.CarType.Type,
                    CargoOwner = cargo.CargoOwner.CargoOwnerName,
                    CargoRequestNumber = cargo.CargoRequestNumber,
                    ClientName = cargo.Client.ClientName
            }))
                .ToArray();

            for (int i = 0; i < shipments.Length; i++)
            {
                shipments[i].Number = i + 1;
            }

            DataGridInfo.ItemsSource = shipments;
        }

        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            var selectedShipment = row.Item as ShipmentViewModel;

            var cargoWindow = new AddEditCargo(selectedShipment.ShipmentId);
            cargoWindow.ShowDialog();

            MakeDataGrid();
        }

        private void ButtonAdd_OnClick(object sender, RoutedEventArgs e)
        {

            var cargoWindow = new AddEditCargo();
            cargoWindow.ShowDialog();

            MakeDataGrid();
        }
    }

}